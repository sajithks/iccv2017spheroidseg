
--data reading
require 'hdf5'
require 'torch'
require 'nn'
require 'optim'
require 'cunn'
require 'cudnn'
require 'nngraph'
-- nt=require 'net-toolkit'
--model cleaning

function zeroDataSize(data)
  if type(data) == 'table' then
    for i = 1, #data do
      data[i] = zeroDataSize(data[i])
    end
  elseif type(data) == 'userdata' then
    data = torch.Tensor():typeAs(data)
  end
  return data
end

-- Resize the output, gradInput, etc temporary tensors to zero (so that the
-- on disk size is smaller)
function cleanupModel(node)
  if node.output ~= nil then
    node.output = zeroDataSize(node.output)
  end
  if node.gradInput ~= nil then
    node.gradInput = zeroDataSize(node.gradInput)
  end
  if node.finput ~= nil then
    node.finput = zeroDataSize(node.finput)
  end
  -- Recurse on nodes with 'modules'
  if (node.modules ~= nil) then
    if (type(node.modules) == 'table') then
      for i = 1, #node.modules do
        local child = node.modules[i]
        cleanupModel(child)
      end
    end
  end
  collectgarbage()
  end
---
local Conv = cudnn.SpatialConvolution
local ReLU = cudnn.ReLU
local Max = nn.SpatialMaxPooling
local Sbatchnorm = nn.SpatialBatchNormalization
local Fconv = cudnn.SpatialFullConvolution
local Dilconv = nn.SpatialDilatedConvolution
-- create conv-batchnorm-relu layer

local function createConvlayer(nInputPlane, nOutputPlane, kernsize, stride, pad)
  clayer = nn.Sequential()
  clayer:add(Conv(nInputPlane, nOutputPlane, kernsize, kernsize, stride, stride, pad, pad))
  clayer:add(Sbatchnorm(nOutputPlane))
  clayer:add(ReLU())
  return clayer
end

-- create conv-batchnorm-relu-maxpool layer

local function createConvPoollayer(nInputPlane, nOutputPlane, kernsize, stride, pad)
  clayer = nn.Sequential()
  clayer:add(Conv(nInputPlane, nOutputPlane, kernsize, kernsize, stride, stride, pad, pad))
  clayer:add(Sbatchnorm(nOutputPlane))
  clayer:add(ReLU())
  clayer:add(Max(2, 2, 2, 2))
  return clayer
end

-- create dilated conv-batchnorm-relu layer

local function createDilatedConvlayer(nInputPlane, nOutputPlane, kernsize, stride, pad, dilsize)
  clayer = nn.Sequential()
  clayer:add(Dilconv(nInputPlane, nOutputPlane, kernsize, kernsize, stride, stride, pad, pad, dilsize, dilsize))
  clayer:add(Sbatchnorm(nOutputPlane))
  clayer:add(ReLU())
  -- clayer:add(Max(2, 2, 2, 2))
  return clayer
end

--residual layer creation

local function createReslayer(input, nInputPlane, nOutputPlane)
  -- input = nn.Identity()()
  layer1 = createConvlayer(nInputPlane, 16, 1, 1, 0)(input)
  layer2 = createConvlayer(16, 16, 3, 1, 1)(layer1)
  layer3 = createConvlayer(16, nOutputPlane, 1, 1, 0)(layer2)
  elemwiseadd = nn.CAddTable()({input, layer3})
  snorm = Sbatchnorm(nOutputPlane)(elemwiseadd)
  -- resmod = nn.gModule({input},{snorm})
  return snorm
end

--residual layer creation2 dynamic size
local function createReslayer2(input, nInputPlane, nOutputPlane)
  -- input = nn.Identity()()
  layer1 = createConvlayer(nInputPlane, nOutputPlane, 1, 1, 0)(input)
  layer2 = createConvlayer(nOutputPlane, nOutputPlane, 3, 1, 1)(layer1)
  layer3 = createConvlayer(nOutputPlane, nOutputPlane, 1, 1, 0)(layer2)
  elemwiseadd = nn.CAddTable()({input, layer3})
  snorm = Sbatchnorm(nOutputPlane)(elemwiseadd)
  -- resmod = nn.gModule({input},{snorm})
  return snorm
end



-- deconv layer
local function createDeconvlayer(input, nInputPlane, nOutputPlane)
  layer1 = Fconv(nInputPlane, nOutputPlane, 2, 2, 2, 2, 0, 0, 0, 0)(input)
  layer2 = Sbatchnorm(nOutputPlane)(layer1)
  layer3 = ReLU()(layer2)
  layer4 = createConvlayer(nOutputPlane,nOutputPlane, 3, 1, 1)(layer3)
  layer5 = createConvlayer(nOutputPlane,nOutputPlane, 3, 1, 1)(layer4)
  return layer5
end

--inception-resnet v1
local function inceptionReslayer1(input, nInputPlane, nOutputPlane)
  -- input = nn.Identity()()
  --Conv(nInputPlane, nOutputPlane, kernsize, kernsize, stride, stride, pad, pad)

  path1layer1 = Conv(nInputPlane, nOutputPlane, 1, 1, 1, 1, 0, 0)(input)
  path1layer2 = Conv(nOutputPlane, nOutputPlane, 1, 3, 1, 1, 0, 1)(path1layer1)
  path1layer3 = Conv(nOutputPlane, nOutputPlane, 3, 1, 1, 1, 1, 0)(path1layer2)

  path2layer1 = Conv(nInputPlane, nOutputPlane, 1, 1, 1, 1, 0, 0)(path1layer2)

  --pathcat = nn.Concat(2)({path2layer1,path1layer3})
  pathcat = nn.JoinTable(2)({path2layer1,path1layer3})
  combinelayer = Conv(nOutputPlane*2, nOutputPlane, 1, 1, 1, 1, 0, 0)(pathcat)

  elemwiseadd = nn.CAddTable()({input, combinelayer})

  snorm = Sbatchnorm(nOutputPlane)(elemwiseadd)
  srelu = ReLU()(snorm)
  -- resmod = nn.gModule({input},{snorm})
  return srelu
end

local function inceptionReslayer1_3by3(input, nInputPlane, nOutputPlane)
  -- replaced 1X3 convolutions with 3X3 convs, added bathnormalization
  --Conv(nInputPlane, nOutputPlane, kernsize, kernsize, stride, stride, pad, pad)

  path1layer1 = Conv(nInputPlane, nOutputPlane, 1, 1, 1, 1, 0, 0)(input)
  path1layer1_snorm = Sbatchnorm(nOutputPlane)(path1layer1)
  path1layer2 = Conv(nOutputPlane, nOutputPlane, 3, 3, 1, 1, 1, 1)(path1layer1_snorm)
  path1layer2_snorm = Sbatchnorm(nOutputPlane)(path1layer2)
  path1layer3 = Conv(nOutputPlane, nOutputPlane, 3, 3, 1, 1, 1, 1)(path1layer2_snorm)
  path1layer3_snorm = Sbatchnorm(nOutputPlane)(path1layer3)

  path2layer1 = Conv(nInputPlane, nOutputPlane, 1, 1, 1, 1, 0, 0)(path1layer3_snorm)
  path2layer1_snorm = Sbatchnorm(nOutputPlane)(path2layer1)

  --pathcat = nn.Concat(2)({path2layer1,path1layer3})
  pathcat = nn.JoinTable(2)({path2layer1_snorm,path1layer3_snorm})
  combinelayer = Conv(nOutputPlane*2, nOutputPlane, 1, 1, 1, 1, 0, 0)(pathcat)

  elemwiseadd = nn.CAddTable()({input, combinelayer})

  snorm = Sbatchnorm(nOutputPlane)(elemwiseadd)
  srelu = ReLU()(snorm)
  -- resmod = nn.gModule({input},{snorm})
  return srelu
end

local function inceptionReslayer2_3by3(input, nInputPlane, nOutputPlane)
  -- replaced 1X3 convolutions with 3X3 convs, added bathnormalization
  --Conv(nInputPlane, nOutputPlane, kernsize, kernsize, stride, stride, pad, pad)
  --additions replaced by combining and 1by1 convs
  path1layer1 = Conv(nInputPlane, nOutputPlane, 1, 1, 1, 1, 0, 0)(input)
  path1layer1_snorm = Sbatchnorm(nOutputPlane)(path1layer1)
  path1layer2 = Conv(nOutputPlane, nOutputPlane, 3, 3, 1, 1, 1, 1)(path1layer1_snorm)
  path1layer2_snorm = Sbatchnorm(nOutputPlane)(path1layer2)
  path1layer3 = Conv(nOutputPlane, nOutputPlane, 3, 3, 1, 1, 1, 1)(path1layer2_snorm)
  path1layer3_snorm = Sbatchnorm(nOutputPlane)(path1layer3)

  path2layer1 = Conv(nInputPlane, nOutputPlane, 1, 1, 1, 1, 0, 0)(input) -- modified here to allow second input path
  path2layer1_snorm = Sbatchnorm(nOutputPlane)(path2layer1)

  --pathcat = nn.Concat(2)({path2layer1,path1layer3})
  pathcat = nn.JoinTable(2)({path2layer1_snorm,path1layer3_snorm})
  combinelayer = Conv(nOutputPlane*2, nOutputPlane, 1, 1, 1, 1, 0, 0)(pathcat)

  catinput = nn.JoinTable(2)({input, combinelayer})
  combineinput = Conv(nOutputPlane*2, nOutputPlane, 1, 1, 1, 1, 0, 0)(catinput)
  snorm = Sbatchnorm(nOutputPlane)(combineinput)
  srelu = ReLU()(snorm)
  -- resmod = nn.gModule({input},{snorm})
  return srelu
end


-- deep path with long skip connections and residual connections
local function deepPath(input, resplane)
  layer3 = createConvlayer(resplane, resplane, 3, 1, 1)(input)
  layer4 = inceptionReslayer1(layer3, resplane, resplane)
  layer5 = inceptionReslayer1(layer4, resplane, resplane)
  layer6 = inceptionReslayer1(layer5, resplane, resplane)
  layer7 = inceptionReslayer1(layer6, resplane, resplane)

  --long skip paths
  layer8 = inceptionReslayer1(layer7, resplane, resplane)
  eadd8 = nn.CAddTable()({layer8, layer6})
  sn8 = Sbatchnorm(resplane)(eadd8)

  layer9 = inceptionReslayer1(sn8, resplane, resplane)
  eadd9 = nn.CAddTable()({layer9, layer7})
  sn9 = Sbatchnorm(resplane)(eadd9)

  layer10 = inceptionReslayer1(sn9, resplane, resplane)
  eadd10 = nn.CAddTable()({layer10, layer6})
  sn10 = Sbatchnorm(resplane)(eadd10)

  layer11 = inceptionReslayer1(sn10, resplane, resplane)
  eadd11 = nn.CAddTable()({layer11, layer5})
  sn11 = Sbatchnorm(resplane)(eadd11)

  layer12 = inceptionReslayer1(sn11, resplane, resplane)
  eadd12 = nn.CAddTable()({layer12, layer4})
  sn12 = Sbatchnorm(resplane)(eadd12)
  return sn12
end

local function deepPath2_3by3(input, resplane)
  layer3 = createConvlayer(resplane, resplane, 3, 1, 1)(input)
  layer4 = inceptionReslayer2_3by3(layer3, resplane, resplane)
  layer5 = inceptionReslayer1_3by3(layer4, resplane, resplane)
  layer6 = inceptionReslayer1_3by3(layer5, resplane, resplane)
  layer7 = inceptionReslayer1_3by3(layer6, resplane, resplane)

  --long skip paths
  layer8 = inceptionReslayer1_3by3(layer7, resplane, resplane)
  eadd8 = nn.CAddTable()({layer8, layer6})
  sn8 = Sbatchnorm(resplane)(eadd8)

  layer9 = inceptionReslayer1_3by3(sn8, resplane, resplane)
  eadd9 = nn.CAddTable()({layer9, layer7})
  sn9 = Sbatchnorm(resplane)(eadd9)

  layer10 = inceptionReslayer1_3by3(sn9, resplane, resplane)
  eadd10 = nn.CAddTable()({layer10, layer6})
  sn10 = Sbatchnorm(resplane)(eadd10)

  layer11 = inceptionReslayer1_3by3(sn10, resplane, resplane)
  eadd11 = nn.CAddTable()({layer11, layer5})
  sn11 = Sbatchnorm(resplane)(eadd11)

  layer12 = inceptionReslayer1_3by3(sn11, resplane, resplane)
  eadd12 = nn.CAddTable()({layer12, layer4})
  sn12 = Sbatchnorm(resplane)(eadd12)
  return sn12
end

local function deepPath3_3by3(input, resplane)
  --all additions repalced by combining and 1by1 convolutions
  layer3 = createConvlayer(resplane, resplane, 3, 1, 1)(input)
  layer4 = inceptionReslayer2_3by3(layer3, resplane, resplane)
  layer5 = inceptionReslayer2_3by3(layer4, resplane, resplane)
  layer6 = inceptionReslayer2_3by3(layer5, resplane, resplane)
  layer7 = inceptionReslayer2_3by3(layer6, resplane, resplane)

  --long skip paths
  layer8 = inceptionReslayer2_3by3(layer7, resplane, resplane)
  eadd8 = nn.JoinTable(2)({layer8, layer6})
  eadd8conv = Conv(resplane*2, resplane, 1, 1, 1, 1, 0, 0)(eadd8)
  sn8 = Sbatchnorm(resplane)(eadd8conv)

  layer9 = inceptionReslayer2_3by3(sn8, resplane, resplane)
  eadd9 = nn.JoinTable(2)({layer9, layer7})
  eadd9conv = Conv(resplane*2, resplane, 1, 1, 1, 1, 0, 0)(eadd9)
  sn9 = Sbatchnorm(resplane)(eadd9conv)

  layer10 = inceptionReslayer2_3by3(sn9, resplane, resplane)
  eadd10 = nn.JoinTable(2)({layer10, layer6})
  eadd10conv = Conv(resplane*2, resplane, 1, 1, 1, 1, 0, 0)(eadd10)
  sn10 = Sbatchnorm(resplane)(eadd10conv)

  layer11 = inceptionReslayer2_3by3(sn10, resplane, resplane)
  eadd11 = nn.JoinTable(2)({layer11, layer5})
  eadd11conv = Conv(resplane*2, resplane, 1, 1, 1, 1, 0, 0)(eadd11)
  sn11 = Sbatchnorm(resplane)(eadd11conv)

  layer12 = inceptionReslayer2_3by3(sn11, resplane, resplane)
  eadd12 = nn.JoinTable(2)({layer12, layer4})
  eadd12conv = Conv(resplane*2, resplane, 1, 1, 1, 1, 0, 0)(eadd12)
  sn12 = Sbatchnorm(resplane)(eadd12conv)
  return sn12
end

local function deepPath4_3by3(input, resplane)
  --all additions repalced by combining and 1by1 convolutions
  layer3 = createConvlayer(resplane, resplane, 3, 1, 1)(input)
  layer4 = inceptionReslayer2_3by3(layer3, resplane, resplane)
  layer5 = inceptionReslayer2_3by3(layer4, resplane, resplane)
  layer6 = inceptionReslayer2_3by3(layer5, resplane, resplane)
  layer7 = inceptionReslayer2_3by3(layer6, resplane, resplane)

  --long skip paths
  layer8 = inceptionReslayer2_3by3(layer7, resplane, resplane)
  eadd8 = nn.JoinTable(2)({layer8, layer6})
  eadd8conv = Conv(resplane*2, resplane, 1, 1, 1, 1, 0, 0)(eadd8)
  sn8 = Sbatchnorm(resplane)(eadd8conv)

  layer9 = inceptionReslayer2_3by3(sn8, resplane, resplane)
  eadd9 = nn.JoinTable(2)({layer9, layer5})
  eadd9conv = Conv(resplane*2, resplane, 1, 1, 1, 1, 0, 0)(eadd9)
  sn9 = Sbatchnorm(resplane)(eadd9conv)

  layer10 = inceptionReslayer2_3by3(sn9, resplane, resplane)
  eadd10 = nn.JoinTable(2)({layer10, layer4})
  eadd10conv = Conv(resplane*2, resplane, 1, 1, 1, 1, 0, 0)(eadd10)
  sn10 = Sbatchnorm(resplane)(eadd10conv)

  return sn10
end
-- foldnum = 1
for foldnum =1,5 do
  ----------------------------------------------------------------------------------------------
  trainfile = <location of a training file containing the location of all hdf5 files>..foldnum..'/train.txt'

  batchsize = 1--30

  ----------------------------------------------------------------------------------------------
  --create model
  real_label = torch.Tensor(batchsize,1,1):fill(2)
  fake_label = torch.Tensor(batchsize,1,1):fill(1)

  convsize1 = 60--8
  convsize2 = convsize1*2--64--16
  convsize3 = convsize1*4
  convsize4 = convsize1*8
  convsize5 = convsize1*16
  convsize6 = convsize1*32

  outfolder = <output folder name>..foldnum.."/gan/level16_8_baseline_fmap"..convsize1
  modelname = "adamlabwt1by1D3"

  --resplane = 64--256--64

  kernsize = 3
  stride = 1
  pad = 1

  dilstride = 2
  dilpad = 2
  dilsize = 2
  ------------------------------------ Segmentation network ----------------------------------------------------------

  -- create segmentation model
  input = nn.Identity()()
  --createConvlayer(nInputPlane, nOutputPlane, kernsize, stride, pad)(input)
  -- down by 2
  layer_by2 = createDilatedConvlayer(1, convsize1, kernsize, dilstride, dilpad, dilsize)(input) --
  -- deepl2 = deepPath(layer_by2, convsize1)
  -- deepl2_up1 = createDeconvlayer(deepl2, convsize1, convsize1)
  -- down by 4
  layer_by4 = createDilatedConvlayer(convsize1, convsize2, kernsize, dilstride, dilpad, dilsize)(layer_by2) --
  -- deepl4 = deepPath(layer_by4, convsize2)
  -- deepl4_up2 = createDeconvlayer(deepl4, convsize2, convsize1)
  -- deepl4_up1 = createDeconvlayer(deepl4_up2, convsize1, convsize1)
  -- down by 8
  layer_by8 = createDilatedConvlayer(convsize2, convsize3, kernsize, dilstride, dilpad, dilsize)(layer_by4) --
  deepl8 = deepPath4_3by3(layer_by8, convsize3)
  deepl8_up4 = createDeconvlayer(deepl8, convsize3, convsize2)
  deepl8_up2 = createDeconvlayer(deepl8_up4, convsize2, convsize1)
  deepl8_up1 = createDeconvlayer(deepl8_up2, convsize1, convsize1)
  -- down by 16
  layer_by16 = createDilatedConvlayer(convsize3, convsize4, kernsize, dilstride, dilpad, dilsize)(layer_by8) --
  deepl16 = deepPath4_3by3(layer_by16, convsize4)
  deepl16_up8 = createDeconvlayer(deepl16, convsize4, convsize3)
  deepl16_up4 = createDeconvlayer(deepl16_up8, convsize3, convsize2)
  deepl16_up2 = createDeconvlayer(deepl16_up4, convsize2, convsize1)
  deepl16_up1 = createDeconvlayer(deepl16_up2, convsize1, convsize1)

  -- combine all layers

  -- combineall = nn.CAddTable()({deepl2_up1, deepl4_up1, deepl8_up1, deepl16_up1})
  -- combineall = nn.JoinTable(2)({deepl2_up1, deepl4_up1, deepl8_up1, deepl16_up1})
  combineall = nn.JoinTable(2)({ deepl8_up1, deepl16_up1})

  combineall_l2 = createConvlayer(convsize1*2, 2, kernsize, 1, pad)(combineall) --

  -- up1_l2 = createConvlayer(convsize1, 2, kernsize, 1, pad)(deepl16_up1) --
  -- up1_l2_lsm = nn.SpatialLogSoftMax()(up1_l2)

  -- model = nn.gModule({input}, {combineall_l2lsm})
  S_model = nn.gModule({input}, {combineall_l2})


  -- combine all layers
  --combineall = nn.CAddTable()({deepl2_up1, deepl4_up1, deepl8_up1, deepl16_up1})
  -- combineall = nn.JoinTable(2)({deepl2_up1, deepl4_up1, deepl8_up1, deepl16_up1})
  -- combineall_l2 = createConvlayer(convsize1*4, 2, kernsize, 1, pad)(combineall) --
  -- combineall_l2lsm = nn.SpatialLogSoftMax()(combineall_l2)
  -- model = nn.gModule({input}, {combineall_l2lsm})

  -- labelweight = torch.Tensor(2)
  -- labelweight[1] = 0.3
  -- labelweight[2] = 1.0
  ------------------------------------ Adversarial network ----------------------------------------------------------
  -- create adversarial model
  convsize1 = 8 --just for this session
  convsize2 = convsize1*2--64--16
  convsize3 = convsize1*4
  convsize4 = convsize1*8
  convsize5 = convsize1*16
  convsize6 = convsize1*32



  D_input = nn.Identity()()
  D_layer1 = createDilatedConvlayer(2, convsize1, kernsize, dilstride, dilpad, dilsize)(D_input) --
  D_layer2 = createConvlayer(convsize1, convsize1, 1, 1, 0)(D_layer1)
  D_layer3 = createDilatedConvlayer(convsize1, convsize2, kernsize, dilstride, dilpad, dilsize)(D_layer2) --
  D_layer4 = createConvlayer(convsize2, convsize2, 1, 1, 0)(D_layer3)
  D_layer5 = createDilatedConvlayer(convsize2, convsize3, kernsize, dilstride, dilpad, dilsize)(D_layer4) --
  D_layer6 = createConvlayer(convsize3, convsize3, 1, 1, 0)(D_layer5)
  D_layer7 = createDilatedConvlayer(convsize3, convsize4, kernsize, dilstride, dilpad, dilsize)(D_layer6) --
  D_layer8 = createConvlayer(convsize4, convsize4, 1, 1, 0)(D_layer7)
  D_layer9 = createDilatedConvlayer(convsize4, convsize5, kernsize, dilstride, 0, 1)(D_layer8) --
  D_layer10 = createConvlayer(convsize5, convsize5, 1, 1, 0)(D_layer9)
  D_layer11 = createDilatedConvlayer(convsize5, convsize6, kernsize, dilstride, 0, 1)(D_layer10) --
  D_layer12 = createConvlayer(convsize6, convsize6, 1, 1, 0)(D_layer11)
  -- D_layer13 = createDilatedConvlayer(convsize6, convsize6, kernsize, dilstride, 0, 1)(D_layer12) --
  D_layer13 = createConvlayer(convsize6, 2, 3, 1, 0)(D_layer12)
  D_layer14 = nn.SpatialAveragePooling(4,4)(D_layer13)
  D_model = nn.gModule({D_input}, {D_layer14})

  ------------------------------------ Criteria ----------------------------------------------------------

  -- S_criterion = nn.SpatialClassNLLCriterionWeighted()
  S_criterion = cudnn.SpatialCrossEntropyCriterionWeighted()
  D_criterion = cudnn.SpatialCrossEntropyCriterion()
  -- D_criterion = nn.BCECriterion()

  S_model:cuda()
  D_model:cuda()

  -- criterion:cuda()

  S_criterion:cuda()
  D_criterion:cuda()

  ------------------------------------ Optimizer configurations ----------------------------------------------------------
  trainLoggerD = optim.Logger(paths.concat(outfolder, 'adv10/'..modelname..'Dadv10.log'))
  trainLoggerS = optim.Logger(paths.concat(outfolder, 'adv10/'..modelname..'Sadv10.log'))
  -- trainLoggerD = optim.Logger(paths.concat(outfolder, 'trainD4labwtadam.log'))
  -- trainLoggerS = optim.Logger(paths.concat(outfolder, 'trainS4labwtadam.log'))

  -- optimStateD = {
  --   learningRate = 1e-4,
  --   alpha = 0.9,
  --  }
  -- optimStateS = {
  --   learningRate = 1e-4,
  --   alpha = 0.9,
  -- }
  optimStateG = {
     learningRate = 1e-4,
     beta1 = 0.5,
  }
  optimStateD = {
     learningRate = 1e-3,
     beta1 = 0.5,
  }

  optimMethod = optim.adam
    --  optimMethod = optim.rmsprop
    --  optimMethod = optim.sgd


  parametersD, gradParametersD = D_model:getParameters()
  parametersS, gradParametersS = S_model:getParameters()


    -- create closure to evaluate f(X) and df/dX of discriminator
  fDx = function(x)
    iteration = iteration or 1

    gradParametersD:zero()
    -- train with real data
    -- real data is the product of raw image and label, both fg and bg
    realdata = torch.Tensor(1,2,480,480):fill(0)
    -- print('call cmul')

    realdata[1][1] = torch.cmul(singledata, singlelabel:eq(2):float())--forground
    realdata[1][2] = torch.cmul(singledata, singlelabel:eq(1):float())-- background
    -- print('cmul done fdx ')

    realdatacuda = realdata:cuda()
    --real data forward pass

    D_realoutput = D_model:forward(realdatacuda)
    -- print(D_realoutput)

    D_realerr = D_criterion:forward(D_realoutput, real_label:cuda())
    --real data backward pass
    D_realdf_do = D_criterion:backward(D_realoutput, real_label:cuda())
    D_model:backward(realdatacuda, D_realdf_do)
    -- train with fake data/segmentation result
    -- forward through segmentor to get fake data
    singledatacuda = singledata:cuda()
    segout = S_model:forward(singledatacuda)
    smax = nn.SpatialSoftMax()
    segoutdouble = smax:forward(segout:double())

    fakedata = torch.Tensor(1,2,480,480):fill(0)
    fakedata[1][1] = torch.cmul(singledata:double(), segoutdouble[1][1])
    fakedata[1][2] = torch.cmul(singledata:double(), segoutdouble[1][2])

    fakedatacuda = fakedata:cuda()
    --fake data forward pass
    D_fakeoutput = D_model:forward(fakedatacuda)
    -- print('D_fakeoutput '..D_fakeoutput[1][1][1][1]..' D_realoutput '..D_realoutput[1][1][1][1])
    D_fakeerr = D_criterion:forward(D_fakeoutput, fake_label:cuda())
    -- print('D_fakeerr'..D_fakeerr)
    --fake data backward pass
    D_fakedf_do = D_criterion:backward(D_fakeoutput, fake_label:cuda())
    D_model:backward(fakedatacuda, D_fakedf_do)

    D_err = D_realerr + D_fakeerr
    trainLoggerD:add{['iteration']=iteration,['D_realerr']=D_realerr,['D_fakeerr']=D_fakeerr}
    iteration = iteration + 1
    return D_err, gradParametersD

  end

  -- print('call fsx ')

  -- create closure to evaluate f(X) and df/dX of segmentor
  fSx = function(x)
    iteratorS = iteratorS or 1

    lambdaval = -1.0
    lambdaval = lambdaval*(iteratorS/(6000*20 )) -- gradually increase adversarial effect
    gradParametersS:zero()
    -- two criteria 1)backward from the discriminator
    --2)spatial cross entropy for pixel wise loss and
    -- fake data was forwarded through the net previously
    D_output = D_model.output -- netD:forward(input) was already executed in fDx, so save computation
    S_Derr = D_criterion:forward(D_output, real_label:cuda())
    --backward through discriminator
    S_Ddf_do = D_criterion:backward(D_output, real_label:cuda())
    S_Ddf_dg = D_model:updateGradInput(realdatacuda, S_Ddf_do )
    S_Ddf_dgdouble= S_Ddf_dg:double()
    S_Ddf_dg_soft = smax:backward(segout:double(),S_Ddf_dgdouble )
    S_Ddf_dg_softcuda = S_Ddf_dg_soft:cuda()
    -- pixel wise loss
    S_output = S_model.output
    S_SCEerr = S_criterion:forward(S_output, labelminibatcuda, labelwtminibatcuda)--spatial cross entropy error
    -- backward pass through segmentor
    S_SCEdf_do = S_criterion:backward(S_output, labelminibatcuda, labelwtminibatcuda)

    Sumerr = S_Derr + S_SCEerr
    SumGrad = S_SCEdf_do + lambdaval*S_Ddf_dg_softcuda
    S_model:backward(singledatacuda, SumGrad)

    trainLoggerS:add{['iteration']=iteratorS,['S_Derr']=S_Derr,['S_SCEerr']=S_SCEerr,['S_Sumerr']=Sumerr}
    iteratorS = iteratorS + 1

    return Sumerr, gradParametersS

  end

  -- training function
  function train()
    -- to do read multiple files and read parts of files for memory constraints
    datafile = io.open(trainfile,"r")
    -- epoch tracker
    epoch = epoch or 1

    filenumber = 1
    for filename in datafile:lines() do

      myFile = hdf5.open(filename, 'r')
      data = myFile:read('/data'):all()
      label = myFile:read('/label'):all()
      -- labelwt = myFile:read('/combinewt'):all()
      labelwt = myFile:read('/labelwt'):all()

      datasize = data:size(1)
      shuffle = torch.randperm(datasize)

      for tt = 1, datasize do
        singledata = nn.utils.addSingletonDimension(data[shuffle[tt]])
        singledatacuda = singledata:cuda()

        singlelabel = label[shuffle[tt]]
        singlelabelwt = labelwt[shuffle[tt]]

        labelminibatcuda = singlelabel:cuda()
        labelwtminibatcuda = singlelabelwt:cuda()
  ---------------------------------------------------------------------------

  ----------------------------------------------------------------------------
        --update descriminator
        optimMethod(fDx, parametersD, optimStateD)
        --update segmentor
        optimMethod(fSx, parametersS, optimStateS)
      end
      -- optimize segmentor
  --     for tt = 1, datasize do
  --       singledata = nn.utils.addSingletonDimension(data[shuffle[tt]])
  --       singledatacuda = singledata:cuda()
  --
  --       singlelabel = label[shuffle[tt]]
  --       singlelabelwt = labelwt[shuffle[tt]]
  --
  --       labelminibatcuda = singlelabel:cuda()
  --       labelwtminibatcuda = singlelabelwt:cuda()
  -- ---------------------------------------------------------------------------
  --
  -- ----------------------------------------------------------------------------
  --       --update descriminator
  --       -- optimMethod(fDx, parametersD, optimStateD)
  --       --update segmentor
  --       optimMethod(fSx, parametersS, optimStateS)
  --     end



    end







    epoch = epoch + 1
  end


  savnum = 1--50--500
  for count =1,2 do
    print('Epoch: '..count)
    train()
    if count%savnum ==0
     then
      --  newmodel = model:clone()
      --  newmodel:clearState()
       D_model:clearState()
       S_model:clearState()

       savenameD =  'adv10/'..modelname..'Dis_'..count.. '.t7'
       filenameD = paths.concat(outfolder,savenameD)
       os.execute('mkdir -p ' .. sys.dirname(filenameD))
       print('==> saving model to '..filenameD)
       torch.save(filenameD, D_model)

       savenameS =  'adv10/'..modelname..'Seg_'..count.. '.t7'
       filenameS = paths.concat(outfolder,savenameS)
       os.execute('mkdir -p ' .. sys.dirname(filenameS))
       print('==> saving model to '..filenameS)
       torch.save(filenameS, S_model)

    end


  end
  D_model = nil
  S_model = nil
end --end fold loop
--------
----------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

